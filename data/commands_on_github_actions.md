Collected by:

```yaml
name: commands
on: [push]
jobs:
  commands:
    name: commands
    runs-on:
      - ubuntu-latest
    steps:
      - run: compgen -c | sort -u -o commands_on_github_actions.txt
      - uses: actions/upload-artifact@v3
        with:
          name: commands_on_github_actions
          path: commands_on_github_actions.txt
```